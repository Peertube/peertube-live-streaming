/**
 *  Copyright (C) 2021 Anthony Chomienne
 *  This program is free software: you can redistribute it and/or modify it under the terms of the
 *  GNU Affero General Public License as published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>
 */

package fr.mobdev.peertubelive.manager

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import fr.mobdev.peertubelive.objects.StreamData

class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, 2) {

    companion object {
        const val DB_NAME: String = "PeerTubeLive.db"
        const val TABLE_CREDS: String = "Credentials"
        const val CREDS_USERNAME: String = "Username"
        const val CREDS_BASE_URL: String = "Instance"
        const val CREDS_CLIENT_ID: String = "ClientID"
        const val CREDS_CLIENT_SECRET: String = "ClientSecret"
        const val CREDS_ACCESS_TOKEN: String = "AccessToken"
        const val CREDS_TOKEN_TYPE: String = "TokenType"
        const val CREDS_EXPIRES: String = "Expires"
        const val CREDS_REFRESH_TOKEN: String = "RefreshToken"
        const val CREDS_REFRESH_EXPIRES: String = "RefreshTokenExpires"
        const val TABLE_STREAM_SETTINGS: String = "Settings"
        const val SETS_TITLE: String = "Title"
        const val SETS_CATEGORY: String = "Category"
        const val SETS_PRIVACY : String = "Privacy"
        const val SETS_LANGUAGE : String = "Language"
        const val SETS_LICENCE : String = "Licence"
        const val SETS_COMMENTS : String = "Comments"
        const val SETS_DOWNLOAD : String = "Download"
        const val SETS_REPLAY : String = "Replay"
        const val SETS_NSFW : String = "Nsfw"
        const val SETS_RESOLUTION: String = "Resolution"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("CREATE TABLE IF NOT EXISTS  $TABLE_CREDS (id INTEGER PRIMARY KEY, $CREDS_USERNAME TEXT, $CREDS_BASE_URL TEXT, " +
                                                                    "$CREDS_CLIENT_ID TEXT, $CREDS_CLIENT_SECRET TEXT, $CREDS_ACCESS_TOKEN TEXT, " +
                                                                    "$CREDS_TOKEN_TYPE TEXT, $CREDS_EXPIRES INTEGER, $CREDS_REFRESH_TOKEN TEXT, $CREDS_REFRESH_EXPIRES INTEGER);")

        db?.execSQL("CREATE TABLE IF NOT EXISTS $TABLE_STREAM_SETTINGS (id INTEGER PRIMARY KEY, $SETS_TITLE TEXT, $SETS_CATEGORY INTEGER, $SETS_PRIVACY INTEGER, " +
                                                                    "$SETS_LANGUAGE TEXT, $SETS_LICENCE INTEGER, $SETS_COMMENTS INTEGER, " +
                                                                    "$SETS_DOWNLOAD INTEGER, $SETS_REPLAY INTEGER, $SETS_NSFW INTEGER, $SETS_RESOLUTION INTEGER);")

        val values = ContentValues()
        values.put("id",1)
        values.put(SETS_TITLE,"Live")
        values.put(SETS_COMMENTS,true)
        values.put(SETS_DOWNLOAD,true)
        values.put(SETS_NSFW,false)
        values.put(SETS_REPLAY,false)
        values.put(SETS_RESOLUTION,StreamData.STREAM_RESOLUTION.p1080.ordinal)
        db?.insert(TABLE_STREAM_SETTINGS,null,values)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("ALTER TABLE $TABLE_CREDS add column $CREDS_REFRESH_EXPIRES INTEGER;")
    }

    fun insert(table: String, values: ContentValues): Long {
        return writableDatabase.insert(table,null,values)
    }

    fun update(table: String, values: ContentValues, whereClause: String?, whereArgs: Array<String?>): Int {
        return writableDatabase.update(table,values,whereClause,whereArgs)
    }

    fun delete(table: String, whereClause: String?, whereArgs: Array<String?>?): Int {
        return writableDatabase.delete(table,whereClause,whereArgs)
    }

    fun query(table: String, columns: Array<String>?, whereClause: String?, whereArgs: Array<String>?): Cursor {
        return readableDatabase.query(table,columns,whereClause,whereArgs,null,null,null)
    }
}