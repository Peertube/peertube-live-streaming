<i>PeerTube Live</i> pozwala Ci transmitować obraz z kamery twojego smartfona na twój kanał PeerTube.

Połącz się z całym fediverse na żywo ze swojego smartfona.
<i>(Musisz posiadać konto na wybranej instancji PeerTube)</i>.

<br><b>Możliwości:</b>

* Działa z każdą instancją PeerTube!
* Obsługuje wiele kont
* Skonfiguruj swoją transmisję jak chcesz (opis, tytuł, widoczność...)
* Przełączaj między przednią i tylną kamerą
* Łatwe w użyciu

PeerTube jest darmową i federacyjną alternatywą dla scentralizowanych platform wideo, stworzoną przez francuską organizację non-profit <a href="https://framasoft.org/" rel="nofollow" target="_blank">Framasoft</a>. Dowiedz się więcej o PeerTube na <a href="https://joinpeertube.org/" rel="nofollow" target="_blank">joinpeertube.org</a>!
