/**
 *  Copyright (C) 2021 Anthony Chomienne
 *  This program is free software: you can redistribute it and/or modify it under the terms of the
 *  GNU Affero General Public License as published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>
 */

package fr.mobdev.peertubelive.activity

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import fr.mobdev.peertubelive.R
import fr.mobdev.peertubelive.databinding.InstanceItemBinding
import fr.mobdev.peertubelive.objects.OAuthData

class AccountAdapter(private var accounts: List<OAuthData>): RecyclerView.Adapter<AccountAdapter.ViewHolder>() {

    var onDeleteAccount: OnDeleteAccount? = null
    var onClickListener: OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<InstanceItemBinding>(LayoutInflater.from(parent.context), R.layout.instance_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val oauthData = accounts[position]
        holder.binding.username.text = oauthData.username
        holder.binding.url.text = oauthData.baseUrl
        holder.pos = position
    }

    override fun getItemCount(): Int {
        return accounts.size
    }

    fun setAccounts(accounts: List<OAuthData>) {
        this.accounts = accounts
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: InstanceItemBinding) : RecyclerView.ViewHolder(binding.root) {
        var pos: Int = 0
        init {
            binding.root.setOnClickListener {
                onClickListener?.onClick(accounts[pos])
            }

            binding.root.setOnLongClickListener {
                onDeleteAccount?.onDeleteAccount(accounts[pos])
                return@setOnLongClickListener true
            }
        }
    }

    interface OnDeleteAccount{
        fun onDeleteAccount(oAuthData: OAuthData)
    }

    interface OnClickListener{
        fun onClick(oAuthData: OAuthData)
    }
}