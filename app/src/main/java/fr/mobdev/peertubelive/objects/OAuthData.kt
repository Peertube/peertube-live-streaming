/**
 *  Copyright (C) 2021 Anthony Chomienne
 *  This program is free software: you can redistribute it and/or modify it under the terms of the
 *  GNU Affero General Public License as published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>
 */

package fr.mobdev.peertubelive.objects

import android.os.Parcel
import android.os.Parcelable

class OAuthData(var baseUrl: String?, var username: String?, var clientId: String?, var clientSecret: String?, var accessToken: String?, var tokenType: String?, var expires: Long, var refreshToken: String?, var refreshTokenExpires: Long) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readString(),
        parcel.readLong()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(baseUrl)
        parcel.writeString(username)
        parcel.writeString(clientId)
        parcel.writeString(clientSecret)
        parcel.writeString(accessToken)
        parcel.writeString(tokenType)
        parcel.writeLong(expires)
        parcel.writeString(refreshToken)
        parcel.writeLong(refreshTokenExpires)
    }

    override fun describeContents(): Int {
        return 0
    }

    fun updateData(oAuthData: OAuthData){
        baseUrl = oAuthData.baseUrl
        username = oAuthData.username
        clientId = oAuthData.clientId
        clientSecret = oAuthData.clientSecret
        accessToken = oAuthData.accessToken
        tokenType = oAuthData.tokenType
        expires = oAuthData.expires
        refreshToken = oAuthData.refreshToken
        refreshTokenExpires = oAuthData.refreshTokenExpires

    }

    companion object CREATOR : Parcelable.Creator<OAuthData> {
        override fun createFromParcel(parcel: Parcel): OAuthData {
            return OAuthData(parcel)
        }

        override fun newArray(size: Int): Array<OAuthData?> {
            return arrayOfNulls(size)
        }
    }
}