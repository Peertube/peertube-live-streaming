/**
 *  Copyright (C) 2021 Anthony Chomienne
 *  This program is free software: you can redistribute it and/or modify it under the terms of the
 *  GNU Affero General Public License as published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>
 */

package fr.mobdev.peertubelive.objects

import android.os.Parcel
import android.os.Parcelable

class StreamSettings(
    val title: String, val channel: Long, val privacy: Int, val category: Int?, val language: String?, val licence: Int?, val description: String?,
    val comments: Boolean, val download: Boolean, val nsfw: Boolean, val saveReplay: Boolean?, val resolution: StreamData.STREAM_RESOLUTION) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readLong(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
         StreamData.STREAM_RESOLUTION.values()[(parcel.readInt())]
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeLong(channel)
        parcel.writeInt(privacy)
        if (category != null) {
            parcel.writeInt(category)
        }
        parcel.writeString(language)
        if (licence != null) {
            parcel.writeInt(licence)
        }
        parcel.writeString(description)
        parcel.writeByte(if (comments) 1 else 0)
        parcel.writeByte(if (download) 1 else 0)
        parcel.writeByte(if (nsfw) 1 else 0)
        if (saveReplay!= null)
            parcel.writeByte(if (saveReplay) 1 else 0)
        parcel.writeInt(resolution.ordinal)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<StreamSettings> {
        override fun createFromParcel(parcel: Parcel): StreamSettings {
            return StreamSettings(parcel)
        }

        override fun newArray(size: Int): Array<StreamSettings?> {
            return arrayOfNulls(size)
        }
    }
}