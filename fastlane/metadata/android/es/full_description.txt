<i>PeerTube Live</i> le ayuda a transmitir la cámara de su dispositivo a su canal de PeerTube.

Conéctate con todo el fediverse en vivo desde tu dispositivo.
<i>(Necesitará una cuenta en la instancia de PeerTube que elija)</i>

<br><b>Características:</b>

* ¡Funciona con cualquier instancia de PeerTube!
* Admite múltiples cuentas
* Configura tu transmisión como quieras (descripción, título, visibilidad ...)
* Cambiar entre cámaras frontal y trasera
* Fácil de usar

PeerTube es una alternativa gratuita y federada a las plataformas de video centralizadas, desarrollada por la organización sin fines de lucro francesa <a href="https://framasoft.org/" rel="nofollow" target="_blank">Framasoft</a>. Obtenga más información sobre PeerTube en <a href="https://joinpeertube.org/" rel="nofollow" target="_blank">joinpeertube.org</a>!
