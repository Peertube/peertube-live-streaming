<i>PeerTube Live</i> vám pomůže vysílat kameru vašeho zařízení na váš kanál PeerTube.

Spojte se s celou federací živě ze svého zařízení.
<i>(Budete potřebovat účet na instanci PeerTube dle vašeho výběru)</i>

<br><b>Funkce:</b>

* Funguje s jakoukoli instancí PeerTube!
* Podporováno více účtů
* Nastavte si svůj stream, jak chcete (popis, název, viditelnost…)
* Přepínání mezi předním a zadním fotoaparátem
* Snadné použití

PeerTube je bezplatná a federovaná alternativa k centralizovaným videoplatformám, kterou vyvinula francouzská nezisková organizace <a href="https://framasoft.org/" rel="nofollow" target="_blank">Framasoft</a>. Další informace o PeerTube naleznete na <a href="https://joinpeertube.org/" rel="nofollow" target="_blank">joinpeertube.org</a>!
