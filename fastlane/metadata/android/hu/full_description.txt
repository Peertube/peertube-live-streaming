A <i>PeerTube Live</i> segít élő adást közvetítenie az eszköze kamerájáról a PeerTube-csatornájára.

Kapcsolódjon az egész födiverzumhoz élőben az eszközéről.
<i>(Egy fiókra lesz szükséges az Ön által választott PeerTube-példányon)</i>

<br><b>Funkciók:</b>

* Bármilyen PeerTube-példánnyal működik!
* Több fiókot is támogat
* Állítsa be úgy az adást, ahogy szeretné (leírás, cím, láthatóság…)
* Váltás az előlapi és hátlapi kamerák között
* Könnyen használható

A PeerTube a centralizált videómegosztó platformok szabad és föderált alternatívája, amelyet a francia nonprofit szervezet, a <a href="https://framasoft.org/" rel="nofollow" target="_blank">Framasoft</a> fejleszt. Tudjon meg többet a PeerTube-ról a <a href="https://joinpeertube.org/" rel="nofollow" target="_blank">joinpeertube.org</a> oldalon!
