# The stream doesn't start and display: "Your livestream ended because of a network problem"

If you're the instance administrators, check that the rtmp/rtmps port are open on your server. By default rtmp 1935, rtmps 1936. Check your production.yaml.
If you're not the adminstrators, ask them if the configuration is ok, before reporting a bug.


